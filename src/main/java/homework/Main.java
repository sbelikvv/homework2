package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        List<Point> points = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int i = 0;


        while (true) {
            System.out.println("Вводим координаты точки х");
            double x = sc.nextDouble();
            System.out.println("Вводим координаты точки y");
            double y = sc.nextDouble();



            points.add(new Point(x,y));
            System.out.println("Желаете добавить еще (1-да 2- нет)");
            int c = sc.nextInt();
            if (c == 2) {
                break;
            }
            i++;

        }
        System.out.println("Вводим координаты центра окружности х");
        double x3 = sc.nextDouble();
        System.out.println("Вводим координаты центра окружности y");
        double y3 = sc.nextDouble();
        System.out.println("Вводим радиус");
        double r = sc.nextDouble();
        Circle circle = new Circle(new Point(x3, y3), r);
        for (int k = 0; k < points.size(); k++) {
            if (points.get(k) != null) {
                boolean flag = circle.containsPoint(points.get(k));
                if (flag) {

                    System.out.println("точки, которые лежат в окружности " + "X" + points.get(k).getX() + " " + "Y" + points.get(k).getY());

                }
            }
        }
    }

}
